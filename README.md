# Oskar
open source key arrangement,
a mobile braille keyboard,

## Copyright
Copyright 2018 Johannes Strelka-Petz, johannes_at_strelka.at

## License
This documentation describes Open Hardware and is licensed under the
CERN Open Hardware Licence v1.2 or Later

You may redistribute and modify this documentation under the terms of the
CERN OHL v.1.2. (http://ohwr.org/cernohl).
This documentation is distributed
WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE.
Please   see   the   CERN   OHL   v.1.2  for   applicable conditions

## Contact
Johannes Strelka-Petz, johannes_at_strelka.at